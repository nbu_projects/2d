﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public Text LabelTimer;
	public Text LabelStart;
	public Text LabelEnd;

	public float GameTime;
	[HideInInspector]
	public float time;

	[HideInInspector]
	public float GameTimeMinutes;
	[HideInInspector]
	public float GameTimeSeconds;

	void Start() {
		StartCoundownTimer();
	}

	void StartCoundownTimer() {
		time = GameTime;
		InvokeRepeating("UpdateTimer", 0.0f, 0.00833f);
	}

	void UpdateTimer() {
		time -= Time.deltaTime;

		GameTimeMinutes = Mathf.Floor(time / 60);
		GameTimeSeconds = (time % 60);
		// string fraction = ((time * 100) % 100).ToString("000");

		if(GameTimeMinutes < -1 && GameTimeSeconds < -1) {
			LabelTimer.text = "00.00";
		} else {
			LabelTimer.text = GameTimeMinutes.ToString("00") + ":" + GameTimeSeconds.ToString("00");
		}

	}
}
