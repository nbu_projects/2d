﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsControl : MonoBehaviour {

	private Character playerCharacter;
	private Slot currentSlot;

	// Start is called before the first frame update
	void Start() {
		playerCharacter = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
		currentSlot = GetComponent<Slot>();
	}

	// Update is called once per frame
	void Update() {
		
	}

	// Items
	// -- Potions

	public void UsePotionAgility() {
		playerCharacter.updateAgility(true, 10);
		Destroy(gameObject);
	}
	
	public void UsePotionIntellect() {
		playerCharacter.updateIntellect(true, 10);
		Destroy(gameObject);
	}
	
	public void UsePotionStrength() {
		playerCharacter.updateStrength(true, 10);
		Destroy(gameObject);
	}
	
	public void UsePotionStamina() {
		playerCharacter.updateStamina(true, 10);
		Destroy(gameObject);
	}
	
	public void UsePotionHealth() {
		bool used = playerCharacter.Heal(20);

		if(used) {
			Destroy(gameObject);
		}
	}
	
	public void UsePotionMana() {
		bool used = playerCharacter.RefreshMana(20);
		
		if(used) {
			Destroy(gameObject);
		}
	}
}
