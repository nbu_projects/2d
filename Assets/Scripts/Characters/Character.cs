﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour {
	[HideInInspector]
	public bool isPlayer;
	[HideInInspector]
	public bool isAlive;

	// Try to hide in inspector for the player
	// Maybe move to the enemyController and expose it there
	public int expReward;

	// Character Stats

	[HideInInspector]
	public int maxHealth;
	[HideInInspector]
	public int currentHealth;

	[HideInInspector]
	public int maxMana;
	[HideInInspector]
	public int currentMana;


	public int stamina;
	public int strength;
	public int agility;
	public int intellect;

	[HideInInspector]
	public int experience;
	[HideInInspector]
	public int level;
	[HideInInspector]
	public int experienceToLevel;


	// Derivative stats
	// public float attackWaitTime;
	[HideInInspector]
	public float startAttackWaitTime;
	private float baseAttackWaitTime = 0.5f;
	[HideInInspector]
	public float attackRange;
	[HideInInspector]
	public int damage;
	// TODO: Add hit chance
	[HideInInspector]
	public float hitChance;
	private float baseHitChance = 0.5f;
	
	
	void Start() {
		if(gameObject.tag == "Player") {
			isPlayer = true;
		} else {
			isPlayer = false;
		}


		level = 1;
		experienceToLevel = 100;
		experience = 0;

		UpdateAttributes();
		currentHealth = maxHealth;
		currentMana = maxMana;

		isAlive = true;
	}

	void Update() {
		CheckForDeath();
	}

// Level up managment
	private void LevelUp(int leftover) {
			level++;
			experienceToLevel += (25 * (experienceToLevel/100));
			experience = leftover;

			stamina += 10;
			UpdateAttributes();
			currentHealth = maxHealth;
			currentMana = maxMana;
			
			strength += 10;
			agility += 10;
			intellect += 10;
	}

	public void GainExperience(int expAmount) {
		if((experience + expAmount) >= experienceToLevel) {
			LevelUp((experience + expAmount) - experienceToLevel);
		} else {
			experience += expAmount;
		}
	}



// Death related
	private void CheckForDeath() {
		if(currentHealth <= 0) {
			Die();
		}
	}


	private void Die() {
		if(isPlayer) {
			gameObject.SetActive(false);
			isAlive = false;
			GetComponent<HUD>().ToggleDeathScreen();
		} else {
			Destroy(gameObject);
			GainExperience(expReward);
		}
	}

// Various

	public void TakeDamage(int damageTaken) {
		currentHealth -= damageTaken;
		// print("HP: " + currentHealth);
	}

	

	public bool Heal(int healAmount) {
		if(currentHealth == maxHealth) {
			return false;
		} else if(currentHealth < maxHealth) {
			if((currentHealth + healAmount) < maxHealth) {
				currentHealth += healAmount;
			} else {
				currentHealth = maxHealth;
			}
			return true;
		} else {
			return false;
		}
	}

	public bool RefreshMana(int refreshAmount) {
		if(currentMana == maxMana) {
			return false;
		} else if(currentMana < maxMana) {
			if((currentMana + refreshAmount) < maxMana) {
				currentMana += refreshAmount;
			} else {
				currentMana = maxMana;
			}
				return true;
		} else {
			return false;
		}
	}

	private void UpdateAttributes() {
		maxHealth = stamina * 10;
		maxMana = intellect * 20;

		damage = strength * 10;
		startAttackWaitTime = baseAttackWaitTime - agility * 0.0015f;
		hitChance = baseHitChance + agility * 0.002f;
	}

	// Stats setters

	public void updateAgility(bool add, int amount) {
		if(add) {
			agility += amount;
		} else {
			agility -= amount;
		}
		UpdateAttributes();
	}
	
	public void updateIntellect(bool add, int amount) {
		if(add) {
			intellect += amount;
		} else {
			intellect -= amount;
		}
		UpdateAttributes();
	}
	
	public void updateStrength(bool add, int amount) {
		if(add) {
			strength += amount;
		} else {
			strength -= amount;
		}
		UpdateAttributes();
	}
	
	public void updateStamina(bool add, int amount) {
		if(add) {
			stamina += amount;
		} else {
			stamina -= amount;
		}
		UpdateAttributes();
	}
}
