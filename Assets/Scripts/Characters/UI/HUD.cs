﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour {

	public Text healthText;
	private float initialHealthValue;
	public Image healthBar_value;

	public Text manaText;
	private float initialManaValue;
	public Image manaBar_value;

	public Text levelText;

	public Text statsText;

	public Text neededToLevelText;

	public GameObject deathScreen;
	public GameObject attributesPanel;


	private float initialXpValue;
	public Image experienceBar_value;
	private Character playerCharacter;

	void Start() {
		playerCharacter = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
		deathScreen.gameObject.SetActive(false);
		attributesPanel.gameObject.SetActive(false);

		initialHealthValue = playerCharacter.maxHealth;
		initialManaValue = playerCharacter.maxMana;
		initialXpValue = playerCharacter.experience;
	}

	void Update() {
		if(Input.GetKeyDown(KeyCode.Tab)) {
			ToggleAttributesPanel();
		}

		// Texts
		if(playerCharacter && playerCharacter.currentHealth > 1) {
			healthText.text = "Life: " + playerCharacter.currentHealth.ToString() + " / " + playerCharacter.maxHealth.ToString();
			manaText.text = "Mana: " + playerCharacter.currentMana.ToString() + " / " + playerCharacter.maxMana.ToString();

			healthBar_value.fillAmount = playerCharacter.currentHealth / initialHealthValue;
			manaBar_value.fillAmount = playerCharacter.currentMana / initialManaValue;


			levelText.text = playerCharacter.level.ToString();
			neededToLevelText.text = "To next level: " + (playerCharacter.experienceToLevel - playerCharacter.experience);
			statsText.text = "Stamina: " + playerCharacter.stamina + " Agility: " + playerCharacter.agility + " Strength: " + playerCharacter.strength + " Intellect: " + playerCharacter.intellect + " Damage: " + playerCharacter.damage + " Attack Delay: " + string.Format("{0:0.00}", playerCharacter.startAttackWaitTime) + " Chance to hit: " + string.Format("{0:0.00}", playerCharacter.hitChance);

			experienceBar_value.fillAmount = playerCharacter.experience / playerCharacter.experienceToLevel;

		} else {
			healthText.gameObject.SetActive(false);
			levelText.gameObject.SetActive(false);
			neededToLevelText.gameObject.SetActive(false);
		}
	}

	public void ToggleDeathScreen() {
		deathScreen.gameObject.SetActive(true);
	}

	void ToggleAttributesPanel() {
		if(!attributesPanel.gameObject.activeSelf) {
			attributesPanel.gameObject.SetActive(true);
		} else {
			attributesPanel.gameObject.SetActive(false);
		}
	}
}






