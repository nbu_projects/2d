﻿// ------------------
// A controller for the animation and movement of the Player
// ------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour {

	public float speed = 5;
	private Animator anim;

	private Rigidbody2D characterRb;
	private Vector2 movementVelocity;
	public Vector2 moveInput;

	public string facing = "left";
	private string previousFacing;



	void Start() {
		characterRb = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
	}

	void Update() {
		GetInput();
		
		DetermineFacing(moveInput);
		Move();
	}

	void GetInput() {
		moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
	}

	void SetAnimationState() {
		if(moveInput[0] != 0 || moveInput[1] != 0) {
			anim.SetBool("isRunning", true);
		} else {
			anim.SetBool("isRunning", false);
		}
	}

	// Movement Control

	void Move() {
		movementVelocity = moveInput.normalized * speed;
		characterRb.MovePosition(characterRb.position + movementVelocity * Time.fixedDeltaTime);

		SetAnimationState();
	}

	void DetermineFacing(Vector2 move) {
		if(move.x < -0.01f) {
			facing = "left";
		} else if(move.x > 0.01f) {
			facing = "right";
		}

		if(previousFacing != facing) {
			previousFacing = facing;
			gameObject.transform.Rotate(0, 180, 0);
		}
	}

	void Awake() {
		previousFacing = facing;
	}
}
