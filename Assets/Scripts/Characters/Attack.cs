﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour {
	
	private Character parent;
	private GameObject attackObject;
	private float attackWaitTime;

	public LayerMask attackables;
	private bool targetIsEnemy;

	void Start() {
		parent = GetComponent<Character>();
		attackWaitTime = parent.startAttackWaitTime;

		if(Mathf.Log(attackables.value, 2) == 10) {
			targetIsEnemy = true;
		} else {
			targetIsEnemy = false;
		}

		CreateAttackField();
	}

	void Update() {
		if(attackWaitTime > 0) {
			attackWaitTime -= Time.deltaTime;
		} else if (attackWaitTime < 0) {
			attackWaitTime = 0;
			print("Attack Ready!");
		}

		if(targetIsEnemy) {
			if(Input.GetMouseButtonDown(0)) {
				PerformMeleeAttack();
			}
			// print("Next attack in: " + attackWaitTime);

		} else {
			PerformMeleeAttack();
		}
	}

	void PerformMeleeAttack() {
		if(attackWaitTime <= 0) {
			Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackObject.transform.position, parent.attackRange, attackables);

			for (int i = 0; i < enemiesToDamage.Length; i++) {
				enemiesToDamage[i].GetComponent<Character>().TakeDamage(parent.damage);
			}

			// Delaying the attack
			attackWaitTime = parent.startAttackWaitTime;
		}
	}

	void CreateAttackField() {
		attackObject = new GameObject("AttackField");
		attackObject.transform.parent = this.gameObject.transform;
		attackObject.transform.localPosition = new Vector3(-0.2f, 0.1f, 0);
		attackObject.transform.localScale = this.gameObject.transform.localScale - new Vector3(parent.attackRange, 0, 0);
	}

	void OnDrawGizmosSelected() {
		Gizmos.color = Color.red;
		if(attackObject) {
			Gizmos.DrawWireSphere(attackObject.transform.position, parent.attackRange);
		}
	}
}