﻿using System;
using UnityEngine;

public class WizardBoss : MonoBehaviour {
	private float CurrentGameTime;
	private Character boss;

	void Start() {
		boss = GetComponent<Character>();
	}

	void Update() {
		CurrentGameTime = FindObjectOfType<GameManager>().GameTimeSeconds;
		Debug.Log(CurrentGameTime);
		if((int)Math.Round(CurrentGameTime) % 5 == 0) { // <----- needs tunning
			boss.stamina += 1;
			boss.strength += 1;
			boss.agility += 1;


		}
	}
}
