﻿// ------------------
// A controller for the animation and movement of an Enemy
// ------------------


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EnemyController : MonoBehaviour {
	private Character playerCharacter;
	private Character thisEnemy;

	private float initialHealthValue;
	public Image enemyHpBar;

	void Awake() {
		thisEnemy = gameObject.GetComponent<Character>();
		initialHealthValue = thisEnemy.maxHealth;
	}

	void Start() {
		playerCharacter = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
		// print("EnemyHP: " + thisEnemy.currentHealth);
	}

	void Update() {
		enemyHpBar.fillAmount = thisEnemy.currentHealth / initialHealthValue;
	}
}
