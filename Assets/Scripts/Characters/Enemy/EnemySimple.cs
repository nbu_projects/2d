﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySimple : MonoBehaviour {
	
	public float speed;
	public float stopDistance;

	private Transform tarTransform;
	private GameObject target;
	private Rigidbody2D thisRb;
	private Vector2 velocity;

	void Start()  {
		thisRb = GetComponent<Rigidbody2D>();

		target = GameObject.FindGameObjectWithTag("Player");
		tarTransform = target.GetComponent<Transform>();
	}

	// Update is called once per frame
	void FixedUpdate() {
		GoToPlayer();
	}

	void GoToPlayer() {
		if(tarTransform) {
			if(Vector2.Distance(transform.position, tarTransform.position) > stopDistance) {
				transform.position = Vector2.MoveTowards(transform.position, tarTransform.position, speed * Time.deltaTime);
			}
		}
	}
}
