﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using System;

public class EnemyAI : MonoBehaviour {

	public Transform target;
	public Transform selfTransform;
	public float speed = 200f;
	public float nextWayPointDistance = 3f;

	private Path path;
	private int currentWaypoint = 0;
	public bool reachedEndOfPath = false;

	private Seeker seeker;
	private Rigidbody2D rb;

	void Start() {
		seeker = GetComponent<Seeker>();
		rb = GetComponent<Rigidbody2D>();
		selfTransform = GetComponent<Transform>();
	
		InvokeRepeating("UpdatePath", 0f, .5f);
	}

	void OnPathComplete(Path p) {
		if(!p.error) {
			path = p;
			currentWaypoint = 0;
		}
	}

	void FixedUpdate() {
		if(path == null) {
			return;
		}
		if(currentWaypoint >= path.vectorPath.Count) {
			reachedEndOfPath = true;
			return;
		} else {
			reachedEndOfPath = false;
		}

		Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;

		Vector2 force = direction * speed * Time.fixedDeltaTime;
		
		rb.AddForce(force);

		float distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]);
		if(distance < nextWayPointDistance) {
			currentWaypoint++;
		}

		if(force.x >= 0.01f) {
			transform.localScale = new Vector3(-1f, 1f, 1f);
		} else if(force.x <= -0.01f) {
			transform.localScale = new Vector3(1f, 1f, 1f);
		}
	}

	void UpdatePath() {
		if(target) {
			if(seeker.IsDone()) {
				seeker.StartPath(rb.position, target.position, OnPathComplete);
			}
		}
	}
}
