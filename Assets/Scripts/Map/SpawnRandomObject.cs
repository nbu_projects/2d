﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRandomObject : MonoBehaviour
{
    [System.Serializable] //Make public class visable
    public class SpawnChance //Contains spawn gameobject info
    {
        public GameObject Prefab;
        public int SpawnRarity;
    }

    public List<SpawnChance> SpawnObsTable = new List<SpawnChance>();
    public int SpawningChance;


    private void Start()
    {
        Generate();
    }

    public void Generate()
    {
        int calculate_SpawnChance = Random.Range(0, 101);
        if (calculate_SpawnChance > SpawningChance)
        {
            return; //do not spawn
        }
        if (calculate_SpawnChance <= SpawningChance)
        {
            int randomObst = 0;
            for (int i = 0; i< SpawnObsTable.Count; i++)
            {
                randomObst += SpawnObsTable[i].SpawnRarity;
            }
            int randomValue = Random.Range(0, randomObst);
            for (int j = 0; j < SpawnObsTable.Count; j++)
            {
                if (randomValue <= SpawnObsTable[j].SpawnRarity)
                {
                    Instantiate(SpawnObsTable[j].Prefab, transform.position, Quaternion.identity);
                    return;
                }
                randomValue -= SpawnObsTable[j].SpawnRarity;
            }
        }
    }


}
