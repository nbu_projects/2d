﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDoorOrWall : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject[] objects;
    
    void Start()
    {
       // int rand = Random.Range(0, objects.Length);
        
        //Instantiate(objects[rand], transform.position, Quaternion.identity);
        if(gameObject.CompareTag("Door"))
        {
            Instantiate(objects[1], transform.position, Quaternion.identity);
            
        }
        else
            Instantiate(objects[0], transform.position, Quaternion.identity);
    }

}
