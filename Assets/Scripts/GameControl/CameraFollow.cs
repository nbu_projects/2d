﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	public Transform playerTransform;
	public Camera cam;

	void Start() {
		cam.orthographicSize = 1.5f;
	}

	void FixedUpdate() {
		if(playerTransform) {
			transform.position = new Vector3(playerTransform.position.x, playerTransform.position.y, transform.position.z);
		}
	}
}
