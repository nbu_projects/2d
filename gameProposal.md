# "Game 603"

**Genre:** Roguelike / RPG

**Examples:**

- Pixel Dungeon
- Ziggurat

**Inspirations:**

- Pixel Dungeon
- Ziggurat
- Diablo 1 & 2
- Darkest Dungeon
- Crypt of the Necrodancer
- Evoland 1 & 2

**Game premise:**

- You control a charecter with inventory and skills. You have a "home"/"rest" location - village, city, house where you store your gear and manage/buy/upgrade skills. You can enter in an "action" location. This location is where you fight different enemies. The enemies have a gear drop chance. Each "action" (dungeon) location can have multiple levels with a boss encounter at the end. Each "action" location could be randomly generated. If the charecter dies in an "action" location he loses progress (experience, gear etc) and gets respawned to the "home" location. There could be different charecter profiles (melee fighter, ranged fighter, magician, rogue).
The difficulty of the game would increase with the player's progress. A multiplication of the player's level, experience, gear points and others would result in stronger enemies and better gear drop.

**Additional ideas:**

- The game could use different scene view types for the sake of variaty. There could be few types of levels, each using a different style of view.
Examples would be a top-down view level (Direct or Slighly angled), like in an old RPG game. A sidescroller level (Direct or/and Angled), a platformer level. (Maybe a short text-based rpg level)
- With a lot of luck, maybe an isometric level - or maybe the home location, could be isometric.
- A fighter boss type (Tekken, Street Fighter) has also been thought about.

**Mechanics:**

- The game can be turn based or dynamic depending on the type of level you get.